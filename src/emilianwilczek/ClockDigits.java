package emilianwilczek;

public class ClockDigits
{
    private int base;
    private int value;

    /**
     * Constructor
     * @param newBase
     */
    public ClockDigits(int newBase)
    {
        base = newBase;
        value = 0;
    }

    /**
     * Value getter
     * @return
     */
    public int getValue()
    {
        return value;
    }

    /**
     * Formats the display to include 0 in front of single digits so that it's always two digits
     * @return
     */
    public String getDisplayValue() {
        if (value < 10) { return "0" + value; }
        return "" + value;
    }

    /**
     * Value setter
     * @param newDigit
     */
    public void setValue(int newDigit)
    {
        if ((newDigit >= 0) && (newDigit < base))
        {
            value = newDigit;
        }
    }

    /**
     *  Increments the value
     */
    public void increase()
    {
        value = (value + 1) % base;
    }
}
