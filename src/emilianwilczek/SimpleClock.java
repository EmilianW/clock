package emilianwilczek;

import java.time.Clock;

public class SimpleClock
{
    private ClockDigits hour;
    private ClockDigits minute;

    /**
     * Updates clock every minute
     */
    public void run()
    {
        try
        {
            while (true)
            {
                Thread.sleep(60 * 1000);
                this.timeIncrease();
            }
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Constructs a clock at default time
     */
    public SimpleClock()
    {
        hour = new ClockDigits(24);
        minute = new ClockDigits(60);

        updateDisplay();
    }

    /**
     * Constructs a clock at set time
     */
    public SimpleClock(int hours, int minutes)
    {
        hour = new ClockDigits(24);
        minute = new ClockDigits(60);

        setTime(hours, minutes);
    }

    /**
     * Increments the hour when minutes reach 00
     */
    public void timeIncrease()
    {
        minute.increase();
        if (minute.getValue() == 0)
        {
            hour.increase();
        }
        updateDisplay();
    }

    /**
     * Sets the time
     * @param hours
     * @param minutes
     */
    public void setTime(int hours, int minutes)
    {
        hour.setValue(hours);
        minute.setValue(minutes);

        updateDisplay();
    }

    /**
     * Prints out the formatted hours and minutes
     */
    public void updateDisplay()
    {
        System.out.println("Time " + hour.getDisplayValue() + ":" + minute.getDisplayValue());
    }
}